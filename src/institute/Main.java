package institute;

import java.util.*;


public class Main {

    
    public static void main(String[] args) {
        Book b1 = new Book("Java Book", "Sherlock Holmes");
        Book b2 = new Book("WebDev Book", "Doctor Watson");
        Book b3 = new Book("Linux", "James Moriarty");
        
        List<Book> books = new ArrayList<Book>();
        books.add(b1);
        books.add(b2);
        books.add(b3);
        
        Library library = new Library(books);
        
        List<Book> bks = library.getTotalBooksInLibrary();
        
        for(Book bk : bks){
            System.out.println("Title: " + bk.title + "\nAuthor: " + bk.author);
        }
    }
    
}
