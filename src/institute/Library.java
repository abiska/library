package institute;

import java.util.*;

public class Library {
    private List<Book> books;
    
    public Library (List<Book>books){
        this.books = books; 
    }
    
    public List<Book> getTotalBooksInLibrary(){
        return books;
    }
}
